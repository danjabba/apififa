    <div>
        <a href="{{route('fifa')}}"> Regresar atras  </a>
        <div class="card-body">
            <div class="table-title">
                <h2>Jugadores</h2>
                <a href="{{route('inicio')}}"> ir a Inicio </a>
                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nombre</th>
                            <th>Posicion</th>
                            <th>Nacionalidad</th>
                            <th>Equipo</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($players as $item)

                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{ $item->name }}</td>
                            <td> {{ $item->position }} </td> <br>
                            <td>{{$item->nation}}</td>
                            <td>{{$item->team}} </td>


                        </tr>


                        @endforeach
                    </tbody>
                </table>
            </div>

        </div>
    </div>

