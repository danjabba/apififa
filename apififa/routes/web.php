<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'App\Http\Controllers\MainController@index')->name('inicio');
Route::get('/array', 'App\Http\Controllers\MainController@array')->name('array');
Route::get('/apififa', 'App\Http\Controllers\MainController@fifa')->name('fifa');
Route::post('/team', 'App\Http\Controllers\MainController@team')->name('team');
Route::get('/jugadores', 'App\Http\Controllers\MainController@jugadores')->name('jugadores');
Route::post('/procesar', 'App\Http\Controllers\MainController@procesar')->name('procesar');

