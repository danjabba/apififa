<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Country
 *
 * @property int $id
 * @property string $name
 * @property string $position
 * @property string $team
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string $nation

 *
 * @package App
 */

class Player extends Model
{
    protected $table ='player';

    protected $fillable = [
        'name',
        'position',
        'team',
        'nation'
	];
}
