<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\Player;

class MainController extends Controller
{
    public function index(){

        return view('index');
    }

    public function array(){

        return view('array');
    }

    public function procesar(Request $request){

        $number = $request->numero;
        $arreglo = [];
        $n = strlen($number);



        for($i=0 ; $i < $n; $i++){

            array_push($arreglo,$number[$i]);


        }
        $sum1 = [];
        $sum2 = [];
        $reve = array_reverse($arreglo);
        $suma = 0;
        $resta = 0;
        $possible = "No es posible dividir (-1)";

        foreach($arreglo as $item){
            $suma += $item;
            array_push($sum1,$suma);
        }

        foreach($reve as $item2){
            $resta += $item2;
            array_push($sum2,$resta);
        }


        foreach($sum1 as $va){

            foreach($sum2 as $vel){

                if($va == $vel){

                    $possible = "Si es posible dividir (1)";
                }

            }

        }

        return ($possible);
    }

    public function fifa(){

       return view('players');
    }


    public function jugadores(){

        $players =  DB::table('player')->get();

        return view('jugadores', compact('players'));
    }


    public function apififa(){

        $url= "https://www.easports.com/fifa/ultimate-team/api/fut/item?page=1";
        $client = new \GuzzleHttp\Client();
        $res = $client->get($url);
        $content = (string) $res->getBody();

        $dataFinal = json_decode($content, true);

        return $dataFinal;

    }

    public function team(Request $request){


        $slug = $request->team;
        $slug = ucwords($slug);

        $data = $this->apififa();

        foreach($data['items'] as $item){

            if($item['club']['name'] == $slug){

                $player = new Player();

                $player->name = $item['name'];
                $player->position = $item['position'];
                $player->nation = $item['nation']['name'];
                $player->team = $item['club']['name'];
                $player->save();

             }

        }

        return redirect()->route('jugadores');
    }


    public function newplayer($dataFinal,$slug){


        foreach($dataFinal['items'] as $item){

            if($item['club']['name'] == $slug){

                $player = new Player();

                $player->name = $item['name'];
                $player->position = $item['position'];
                $player->nation = $item['nation']['name'];
                $player->team = $item['club']['name'];
                $player->save();

             }

        }

    }


}


